document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'interactive') {
       document.querySelector(".slider").style.visibility="hidden";
       document.getElementsByClassName("fade")[1].style.visibility="hidden";
  } else if (state == 'complete') {
      setTimeout(function(){
         document.getElementById('load').style.visibility="hidden";
         document.querySelector(".slider").style.visibility="visible";
         document.getElementsByClassName("fade")[1].style.visibility="visible";
      }, 4000);
  }
}

var slider = document.querySelector(".slider");
var slides = document.getElementsByClassName("slide");	
var dots = document.getElementsByClassName("dot");

var prevArrow = document.querySelector(".prev");
var nextArrow = document.querySelector(".next");

var slideIndex = 1;
var i;

function plusSlides(n) {
	showSlides((slideIndex += n));
}

function currentSlide(n) {
	if (n != slideIndex)
		showSlides((slideIndex = n));
}

function showSlides(n) {
	prevArrow.style.display = "block";
	nextArrow.style.display = "block";

	if (n >= slides.length) {
		slideIndex = slides.length;
		nextArrow.style.display = "none"; 
	}

	if (n <= 1) {
		slideIndex = 1;
		prevArrow.style.display = "none"; 
	}

	for (i = 0; i < slides.length; i++) {
		slides[i].className = slides[i].className.replace("active-slide", "");
	}

	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace("active-dot", "");
		dots[i].className = dots[i].className.replace("zoom-in", "");
	}

	slides[slideIndex - 1].classList.add("active-slide");

	dots[slideIndex - 1].classList.add("active-dot");
	dots[slideIndex - 1].classList.add("zoom-in");

	if (slideIndex < slides.length) {
		slider.style.backgroundPosition = (slideIndex - 1) * 12.5 + "% top";
		slides[slideIndex - 1].classList.add("fade");
	}
	else {
		slider.style.backgroundPosition = "114% top";
		slides[slideIndex - 1].querySelector(".text").classList.add("slide-in");
		slides[slideIndex - 1].querySelector(".numbertext").classList.add("slide-in");
	}
}

/* Using arrow keys */
function keyUpHandler(event) {
	switch (event.key) {
		case "ArrowLeft":
			if (slideIndex > 1) plusSlides(-1);
			break;
		case "ArrowRight":
			if (slideIndex < slides.length) plusSlides(1);
			break;
	}
}

document.addEventListener('keyup', keyUpHandler);